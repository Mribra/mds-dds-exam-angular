FROM node:16.13.1 as build
WORKDIR /usr/src/app
COPY . .
RUN npm install -g @angular/cli 
RUN npm install

RUN ng build

FROM nginx:latest
COPY --from=build /usr/src/app/dist/mds-dds-exam-angular /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

#commandes :
#docker build -t image_front .
#docker run -it -p 8081:80 image_front
#localhost:8081 navigateur
