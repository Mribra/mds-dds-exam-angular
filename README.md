## Init your env
Run `npm install -g @angular/cli`
Run `npm install`

## Development server

Run `ng serve --port 8081` for a dev server. Navigate to `http://localhost:8081/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


Docker commandes : 1) docker build -t image_front .
 2) docker run -it -p 8081:80 image_front
 3) localhost:8081 dans le navigateur

Le projet est relié à la base de données amazon. Pour changer cette variable, modifier le fichier environment.ts

Deploy Heroku via dockerfile:

git add .
git commit -m "commit"
heroku stack:set container
git push heroku main
